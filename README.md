# Portfolio Containing Basic Cisco Routing Configurations and Network Automation Python Scripts

1. The `cisco-routing-basics` folder contains the Cisco basic routing configurations I have practiced from Mnet Solutions. The activities includes general configuration and static default routing. Open the [cisco-routing-basics](https://gitlab.com/nylrejco/cisco_python/-/tree/master/cisco-routing-basics) folder to view more information.

2. The `python-gns3` folder contains Python scripts created with the courses from David Bombal using a network simulator to automate configuration through Telnet, Paramiko, and Netmiko libraries. Open the [python-gns3](https://gitlab.com/nylrejco/cisco_python/-/tree/master/python-gns3) folder to view more details about the scripts and how they work.
