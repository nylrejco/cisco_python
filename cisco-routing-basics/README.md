# Cisco Basic Routing Configurations

### General Cisco Configuration Activity

#### The instructions for the first activity is to configure the general configurations on each router with the following settings:

1. All passwords must be encrypted
2. Disable ide-timers on all routers


- Router 1 Configurations
| Hostname: R1
| Enable Password: ciscopass
| Console Password: consolepass
| Line Vty Password: telnetpass
| Banner: R1 Router

- Router 2 Configurations
| Hostname: R2
| Enable Password: r2pass
| Console Password: r2consolepass
| Line Vty Password: r2vtypass
| Banner: R2 Router

- Router 3 Configurations
| Hostname: R3
| Enable Password: r3password
| Console Password: r3conpass
| Line Vty Password: r3vtypass
| Banner: R3 Router


Here's the topology used in this activity:

![topology](general_config_topology.PNG)

View full answer for the activity in the `general_config_answers` text file.
Preview of the `general_config_answers`:
```
R1:
enable 
conf t
hostname R1
service password-encryption
enable secret ciscopass
line console 0
login
password consolepass
exec-timeout 0 0 
exit
line vty 0 4
login
password telnetpass
exec-timeout 0 0 
exit
banner login $ R1 Router $
interface serial 0/0
ip address 172.51.32.2 
255.255.255.0
no shut
exit

...
```


### Static Default Routing Activity

#### The instructions for the second activity are:


1. Configure these General Router Configurations on each router:
a. hostname
b. enable password
c. line console
d. line vty
All password must be: cisco
e. banners

2. Configure IP Addresses on each router as seen on the photo. Clock rate must be 128000.

3. Configure static routing on all routers. All routers must be able to ping each other's IP Addresses after configuration.

4. Configure default routing on R1 and R3. Remove the static route configurations on R1 and R3 first. All routers must still be able to ping each other's IP Addesses.

![question](static_default_routing_questions.png)

Here's the topology used in this activity:

![topology](static_default_routing_topology.PNG)

View full answer for the activity in the `static_default_routing_answers` text file. Preview of the answer:
```
1) Hostname, password, line console, line vty

R1:
enable
conf t
hostname R1
enable password cisco
line console 0
login
password cisco
exec-timeout 0 0 
exit
line vty 0 4
login
password cisco
exec-timeout 0 0 
exit
banner login $ R1 $
exit

...
```


*Note: The activities in this folder are those that I have completed from Mnet Solutions.*