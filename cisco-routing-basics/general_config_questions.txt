Configure the General Configurations on each router with the following settings:

1. All passwords must be encrypted
2. Disable ide-timers on all routers


Router 1 Configurations
Hostname: R1
Enable Password: ciscopass
Console Password: consolepass
Line Vty Password: telnetpass
Banner: R1 Router

Router 2 Configurations
Hostname: R2
Enable Password: r2pass
Console Password: r2consolepass
Line Vty Password: r2vtypass
Banner: R2 Router

Router 3 Configurations
Hostname: R3
Enable Password: r3password
Console Password: r3conpass
Line Vty Password: r3vtypass
Banner: R3 Router