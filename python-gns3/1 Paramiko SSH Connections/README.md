# Switch Configuration Using Paramiko Library and Network Simulator

This is a script for establishing connections using the paramiko library. When running the 
`paramiko_ssh` python file after all the configurations has been applied, this will
configure multiple VLANs, loopback IP address, and OSPF. 

The topology used in this simple network automation script is in this photo:

![topology](param_topology.png)

View full script in the `paramiko_ssh` python file.
Preview of the script which also describes the functions of the lines:
```
import paramiko
import time

#Enter the IP Address configured in the switch

ip_address = "192.168.122.106"
username = "nylre"
password = "cisco"
 
#Connects using SSH and accept public key from switch

ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname=ip_address,username=username,password=password)


print "Successful connection", ip_address

#Send shell commands to switch

remote_connection = ssh_client.invoke_shell()

...
```


The output after running the `paramiko_ssh` python file:

1. It will first configure the loopback and ospf on the switch.

	![Loopback_OSPF](output_loop_ospf.PNG)

2. Then it will create VLANs 2 to 20. The number of VLANs to create can be changed in the script.

	![VLANs](output_vlan.PNG)


Configurations made on the devices are on the `configurations` text file.

