#!/usr/bin/env python


import paramiko
import time

#Enter the IP Address configured in the switch

ip_address = "192.168.122.106"
username = "nylre"
password = "cisco"
 
#Connects using SSH and accept public key from switch

ssh_client = paramiko.SSHClient()
ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client.connect(hostname=ip_address,username=username,password=password)


print "Successful connection", ip_address

#Send shell commands to switch

remote_connection = ssh_client.invoke_shell()

#Configure loop back and enable ospf

remote_connection.send("configure terminal\n")
remote_connection.send("int loop 0\n")
remote_connection.send("ip address 1.1.1.1 255.255.255.255\n")
remote_connection.send("int loop 1\n")
remote_connection.send("ip address 2.2.2.2 255.255.255.255\n")
remote_connection.send("router ospf 1\n")
remote_connection.send("network 0.0.0.0 255.255.255.255 area 0\n")

#Create VLAN 2 to 20

for v in range (2,21):
    print "Creating VLAN " + str(v)
    remote_connection.send("vlan " + str(v) +  "\n")
    remote_connection.send("name Python_VLAN_" + str(v) +  "\n")
    time.sleep(0.5)

remote_connection.send("end\n")

#Output information to the console

time.sleep(1)
output = remote_connection.recv(65535)
print output

ssh_client.close