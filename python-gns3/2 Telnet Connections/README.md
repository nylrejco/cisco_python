# Switch and Router Configuration Using Telnet Library and Network Simulator

These are scripts for establishing connections using the python telnet library while also automating basic router and switch configurations.

### Router and Switch Configuration with a Simple Topology

The topology used in this simple network automation script was created through GNS3:

![first_topology](first_topology.png)

When running the `first_R1config` python file, the following steps will be performed by the script:

1. Create a telnet connection to the router.
2. Ask for user credentials such as username and password.
3. Send commands to the router that will enter the routers configuration mode and create loopback addresses and OSPF.

View full script in the `first_R1config` python file. Here's a preview of the script:

```
#!/usr/bin/env python

#This is a python script for telnet connection with router

import getpass
import sys
import telnetlib

#Connects to the router

HOST = "192.168.122.105"
user = raw_input("Enter your telnet username: ")
password = getpass.getpass()

#Creates TELNET connection

tn = telnetlib.Telnet(HOST)

#Asks for the username and password

tn.read_until("Username: ")
tn.write(user + "\n")
if password:
    tn.read_until("Password: ")
    tn.write(password + "\n")


...

```


When running the `first_S1config` python file, the following steps will be performed by the script:

1. Create a telnet connection to the switch.
2. Ask for user credentials such as username and password.
3. Send commands to the router that will enter the routers configuration mode and create VLANs and save the configurations of the switch.

View full script in the `first_S1config` python file. Here's a preview of the script:

```
...


#Enter config mode of switch

tn.write("conf t\n")

#Create VLANs from 2 to 9

for i in range(2,10):
	tn.write("vlan " + str(i) + "\n")
	tn.write("name Python_VLAN_" + str(i) + "\n")

#Close and save configurations

tn.write("end\n")
tn.write("wr\n")
tn.write("exit\n")


print tn.read_all()


...

```


### Configuration of Multiple Switches and Saving Configurations to a File:

The topology used in this in this script was created through GNS3:

![second_topology](second_topology.png)

*Photo credits to David Bombal.*

When running the `second_vlans` python file, the following steps will be performed by the script:

1. Ask for user credentials such as username and password.
2. Open a file that contains the list of switches that needs to be configured.
3. Establish telnet connection to the switches (while prompting for password before entering the switch) that are on the list.
4. Send commands to the switches that will enter the configuration mode and create VLANs to each switch.

View full script in the `second_vlans` python file. Here's a preview of the script:

```
...

#Open file with switch ip addresses

sw = open('myswitches')

#Connect to each line of switch in the file

for line in sw:
	print "Configuring Switch " + (line)
	HOST = line
	tn = telnetlib.Telnet(HOST)

	tn.read_until("Username: ")
	tn.write(user + "\n")

#Ask for password for each Switch after telnet connection

	if password:
	    tn.read_until("Password: ")
	    tn.write(password + "\n")

	tn.write("conf t\n")


...

```

When running the `second_saveconfigs` python file, the following steps will be performed by the script:

1. Ask for user credentials such as username and password.
2. Open a file that contains the list of switches that needs to be configured.
3. Get the running configuration of the switches and save it to a file.


View full script in the `second_saveconfigs` python file. Here's a preview of the script:

```
...

#Ask for credentials  for each Switch after telnet connection

	tn.read_until("Username: ")
	tn.write(user + "\n")

	if password:
	    tn.read_until("Password: ")
	    tn.write(password + "\n")

#Show whole config of switch 

	tn.write("terminal length 0\n")
	tn.write("show run\n")
	tn.write("exit\n")

#Create a file where we can save the config

	readconfig = tn.read_all()
	saveconfig = open("switch" + HOST, "w")
	saveconfig.write(readconfig)
	saveconfig.close


...

```


Manual configurations done on the devices are in the `configurations` text file.