#!/usr/bin/env python

#This is a python script for telnet connection with router

import getpass
import sys
import telnetlib

#Connects to the router

HOST = "192.168.122.105"
user = raw_input("Enter your telnet username: ")
password = getpass.getpass()

#Creates TELNET connection

tn = telnetlib.Telnet(HOST)

#Asks for the username and password

tn.read_until("Username: ")
tn.write(user + "\n")
if password:
    tn.read_until("Password: ")
    tn.write(password + "\n")

#Sends command to the router

tn.write("enable\n")
tn.write("cisco\n")
tn.write("conf t\n")
tn.write("int loop 0\n")
tn.write("ip address 1.1.1.1 255.255.255.255\n")
tn.write("int loop 1\n")
tn.write("ip address 2.2.2.2 255.255.255.255\n")
tn.write("router ospf 1\n")
tn.write("ip address 0.0.0.0 255.255.255.255 area 0\n")
tn.write("end\n")
tn.write("wr\n")
tn.write("exit\n")


print tn.read_all()