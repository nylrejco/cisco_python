#!/usr/bin/env python

#This is a telnet connection for configuring switch

import getpass
import sys
import telnetlib

#Connect to the switch

HOST = "192.168.122.106"
user = raw_input("Enter your telnet username: ")
password = getpass.getpass()

#Establish telnet connection

tn = telnetlib.Telnet(HOST)

#Let admin input username and password

tn.read_until("Username: ")
tn.write(user + "\n")
if password:
    tn.read_until("Password: ")
    tn.write(password + "\n")

#Enter config mode of switch

tn.write("conf t\n")

#Create VLANs from 2 to 9

for i in range(2,10):
	tn.write("vlan " + str(i) + "\n")
	tn.write("name Python_VLAN_" + str(i) + "\n")

#Close and save configurations

tn.write("end\n")
tn.write("wr\n")
tn.write("exit\n")


print tn.read_all()