#!/usr/bin/env python

#This script will save the router configs

import getpass
import sys
import telnetlib

#Ask for username and password

user = raw_input("Enter your telnet username: ")
password = getpass.getpass()

#Open file with switch ip addresses

sw = open('myswitches')

#Connect to each line of switch in the file

for line in sw:
	print "Get running config from Switch " + (line)
	HOST = line.strip()
	tn = telnetlib.Telnet(HOST)


#Ask for credentials  for each Switch after telnet connection

	tn.read_until("Username: ")
	tn.write(user + "\n")

	if password:
	    tn.read_until("Password: ")
	    tn.write(password + "\n")

#Show whole config of switch 

	tn.write("terminal length 0\n")
	tn.write("show run\n")
	tn.write("exit\n")

#Create a file where we can save the config

	readconfig = tn.read_all()
	saveconfig = open("switch" + HOST, "w")
	saveconfig.write(readconfig)
	saveconfig.close
