#!/usr/bin/env python

#This script will create VLANs for Switches

import getpass
import sys
import telnetlib


user = raw_input("Enter your telnet username: ")
password = getpass.getpass()

#Open file with switch ip addresses

sw = open('myswitches')

#Connect to each line of switch in the file

for line in sw:
	print "Configuring Switch " + (line)
	HOST = line
	tn = telnetlib.Telnet(HOST)

	tn.read_until("Username: ")
	tn.write(user + "\n")

#Ask for password for each Switch after telnet connection

	if password:
	    tn.read_until("Password: ")
	    tn.write(password + "\n")

	tn.write("conf t\n")

#Create a number of VLANs for each switch depending on range set

	for v in range(2,10):
		tn.write("vlan " + str(v) + "\n")
		tn.write("name Python_VLAN_" + str(v) + "\n")

#Closes and saves the configurations

	tn.write("end\n")
	tn.write("wr\n")
	tn.write("exit\n")

	print tn.read_all()
