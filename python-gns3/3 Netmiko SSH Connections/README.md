# Switch Configuration Using Netmiko Library and Network Simulator

These are scripts for establishing connections using the python netmiko library and automating switch configurations.

### Multiple Switch Configuration

The topology used in this simple network automation script was created through GNS3:

![first_net_topology](first_net_topology.png)

*Photo credits to David Bombal.*

When running the `second_cisco_guide` python file, the following steps will be performed by the script:

1. Open the `iosv_l2_cisco_design` and `iosv_l2_core_design` file which contains all the configurations needed for the switches.
2. Connect to all the switches that is specified and will automatically accept the username and password that is included in the device properties.
3. Apply all the commands from the opened files in step 1 to all the switches.

View full script in the `second_cisco_guide` python file. Here's a preview of the script:

```
...

#Open this file which contains all the commands for switch configuration

with open('iosv_l2_cisco_design') as f:
    lines = f.read().splitlines()
print lines

#Variable that contains all the devices
#In reverse order because topology is inband management network

all_devices = [iosv_l2_s5, iosv_l2_s4, iosv_l2_s3]

with open('iosv_l2_core_design') as f:
    lines = f.read().splitlines()
print lines

all_devices = [iosv_l2_s2, iosv_l2_s1]


#Connect to all devices and send commands to switch

for device in all_devices:
    net_connect = ConnectHandler(**device)
    output = net_connect.send_config_set(lines)
    print output

...
```



### Configuration of Multiple Switches in Accordance to the Cisco Design Guide:

The topology used in this in this script was created through GNS3:

![second_net_topology](second_net_topology.png)

*Photo credits to David Bombal.*

When running the `second_cisco_guide` python file, the following steps will be performed by the script:

1. Ask for user credentials such as username and password.
2. Open a file that contains the list of switches that needs to be configured.
3. Establish telnet connection to the switches (while prompting for password before entering the switch) that are on the list.
4. Send commands to the switches that will enter the configuration mode and create VLANs to each switch.

View full script in the `second_cisco_guide` python file. Here's a preview of the script:

```
...

#Open file with switch ip addresses

sw = open('myswitches')

#Connect to each line of switch in the file

for line in sw:
	print "Configuring Switch " + (line)
	HOST = line
	tn = telnetlib.Telnet(HOST)

	tn.read_until("Username: ")
	tn.write(user + "\n")

#Ask for password for each Switch after telnet connection

	if password:
	    tn.read_until("Password: ")
	    tn.write(password + "\n")

	tn.write("conf t\n")


...

```


Manual configurations done on the devices are in the `configurations` text file.