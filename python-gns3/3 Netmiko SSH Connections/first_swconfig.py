#!/usr/bin/env python

#This script is for configuration of MORE THAN ONE switch

from netmiko import ConnectHandler

#Specify devices

iosv_l2_s1 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.106',
    'username': 'nylre',
    'password': 'cisco',
}

iosv_l2_s2 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.107',
    'username': 'nylre',
    'password': 'cisco',
}

iosv_l2_s3 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.108',
    'username': 'nylre',
    'password': 'cisco',
}

#Variable that contains all the devices

all_devices = [iosv_l2_s1, iosv_l2_s2, iosv_l2_s3]


#Connect to all devices and print commands

for device in all_devices:
	net_connect = ConnectHandler(**device)
	#net_connect.find_prompt()


#Configure VLANs

	for v in range (2,21):
    	print "Creating VLAN " + str(v)
    	config_commands = ['vlan ' + str(v), 'name Python_VLAN_' + str(v)]
    	output = net_connect.send_config_set(config_commands)
    	print output 