#!/usr/bin/env python

#This script is for configuration of more than one switch
#The commands follows the cisco design guide

from netmiko import ConnectHandler

#Specify devices

iosv_l2_s1 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.105',
    'username': 'nylre',
    'password': 'cisco',
}

iosv_l2_s2 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.106',
    'username': 'nylre',
    'password': 'cisco',
}

iosv_l2_s3 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.107',
    'username': 'nylre',
    'password': 'cisco',
}

iosv_l2_s4 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.108',
    'username': 'nylre',
    'password': 'cisco',
}

iosv_l2_s5 = {
    'device_type': 'cisco_ios',
    'ip': '192.168.122.109',
    'username': 'nylre',
    'password': 'cisco',
}

#Open this file which contains all the commands for switch configuration

with open('iosv_l2_cisco_design') as f:
    lines = f.read().splitlines()
print lines

#Variable that contains all the devices
#In reverse order because topology is inband management network

all_devices = [iosv_l2_s5, iosv_l2_s4, iosv_l2_s3]

with open('iosv_l2_core_design') as f:
    lines = f.read().splitlines()
print lines

all_devices = [iosv_l2_s2, iosv_l2_s1]


#Connect to all devices and send commands to switch

for device in all_devices:
    net_connect = ConnectHandler(**device)
    output = net_connect.send_config_set(lines)
    print output

