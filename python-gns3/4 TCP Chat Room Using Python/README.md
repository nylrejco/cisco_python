# Simple TCP Chat Room Using Python with Kick and Ban Features

This is a simple chat room that you can run using command prompt. With only the `server` and `client` python scripts, you can send messages to other users and receive messages from them as well. I have coded these scripts along with Neural Nine.

### Using the TCP Chat Room with the Kick Feature

To use the TCP Chat Room you must perform the following steps:

1. Open command prompt and run the `server` python file. This will show a "Server is listening.." message to let you know that the server is now running.
2. Open a second command prompt and run the `client` python file. Enter "admin" as the nickname and "adminpass" as the password. This will be the admin client.
3. Open a third command prompt and run the `client` python file again. Enter any nickname you want except for admin. This will be the client.
4. You can now start sending messages using both the admin client and the other client.
5. To kick someone from the chat room, use the admin client and enter the "/kick nickname" command. The nickname after the /kick should be the nickname entered on step 3.


View full script with descriptions in the `server` and `client` python file.


Here's the output after following steps 1 to 5:

- Server side of the chat room.
![server](server.JPG)


- Admin and client side of the chat room.
![client_kick](client_kick.JPG)


### Using the TCP Chat Room with the Ban Feature

This is a continuation of the first part after the client has been kicked:

1. After the client has been kicked, close the command prompt for that client only.
2. Open another command prompt and run the `client` python file again. Choose a nickname similar to the one used before getting kicked. Notice that you were able to rejoin the chat room.
3. Use the admin command prompt and enter the command "/ban nickname". This will add the nickname to the list of banned names in the `bans` text file.
4. Close the banned client command promt and rejoin the chat by running the `client` python file on the command prompt again using the same nickname.
5. Notice that after you used the same nickname, you are not able to connect anymore.


Here's the output after following the steps above for testing the ban feature:

- Server side of the chat room.
![server_ban](server_ban.JPG)

- Admin and client side of the chat room.
![client_ban](client_ban.JPG)

- Banned client attempting to rejoin.
![client_ban_rejoin](client_ban_rejoin.JPG)