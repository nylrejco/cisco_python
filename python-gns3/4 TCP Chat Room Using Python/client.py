import socket
import threading

#Let client input desired nickname

nickname = input('Create your nickname: ')

#Check if the nickname is admin
if nickname == 'admin':
	password = input("Enter your password:")

#Establish server connection

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(('127.0.0.1', 55555))


stop_thread = False

#Client receives messages and checks what code it matches

def client_receive():
	while True:
		global stop_thread
		if stop_thread:
			break
		try:
			message = client.recv(1024).decode('ascii')
			#Check if message code is for nickname:
			if message == 'NICK':
				client.send(nickname.encode('ascii'))
				next_message = client.recv(1024).decode('ascii')

				#Check if message code is for password
				if next_message == 'PASS':
					client.send(password.encode('ascii'))

					#Do not connect client if password is wrong
					if client.recv(1024).decode('ascii') == 'REFUSE':
						print("Connection was refused. Incorrect password.")
						stop_thread = True

				elif next_message == 'BAN':
					print('Connection refused. You are banned.')
					client.close()
					stop_thread = True

#Print message if it is not a code

			else:
				print(message)

#If there is error, close client connection
		except:
			print('An error occurred.')
			client.close()
			break

#Format for sending message

def client_send():
	while True:
#If server refuses the connection, the sending will terminate
		if stop_thread:
			break
		message = f'{nickname}: {input("")}'

		#Check if message contains admin commands
		if message[len(nickname)+2:].startswith('/'):
			if nickname == 'admin':
				#Send corresponding code and nickname to server
				if message[len(nickname)+2:].startswith('/kick'):
					client.send(f'KICK {message[len(nickname)+2+6:]}'.encode('ascii'))
				elif message[len(nickname)+2:].startswith('/ban'):
					client.send(f'BAN {message[len(nickname)+2+5:]}'.encode('ascii'))

			#If the nickname is not admin
			else:
				print("Commands can only be executed by authorized admin")

		#Send to server if it is just an ordinary message		
		else:
			client.send(message.encode('ascii'))

#Thread the target function so this all happens simultaneously

receive_thread = threading.Thread(target=client_receive)
receive_thread.start()

send_thread = threading.Thread(target=client_send)
send_thread.start()

