import threading
import socket

host = '127.0.0.1' 
port = 55555

#Turn on server

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host, port))
server.listen()

clients = []
nicknames = []

#Send message to all connected in server

def send_all(message): 
	for client in clients:
		client.send(message)

#Server should let client should continue receiving message

def each_client(client):
	while True:
		try:
			msg = message = client.recv(1024)
			#Check if admin client sent KICK code
			if msg.decode('ascii').startswith('KICK'):
				if nicknames[clients.index(client)] == 'admin':
					name_to_kick = msg.decode('ascii')[5:]
					#Call function to kick user
					kick_user(name_to_kick)
				else:
					client.send('Command was refused.'.encode('ascii'))

			#Check if admin client sent BAN code
			elif msg.decode('ascii').startswith('BAN'):
				if nicknames[clients.index(client)] == 'admin':
					name_to_ban = msg.decode('ascii')[4:]
					kick_user(name_to_ban)

					#Add banned user to file with list of banned users
					with open('bans.txt', 'a') as banfile:
						banfile.write(f'{name_to_ban}\n')
					print(f'{name_to_ban} was banned.')
				else:
					client.send('Command was refused.'.encode('ascii'))
			else:
				send_all(message)

		#Remove client if causing error
		except: 
			if client in clients:
				index = clients.index(client)
				clients.remove(client)
				client.close()
				nickname = nicknames[index]
				send_all(f'{nickname} left the room.'.encode('ascii'))
				nicknames.remove(nickname)
				break


#Accept all connections

def receive(): 
 	while True:
 		client, address = server.accept()
 		print(f'Connected with {str(address)}')

 		#NICK is keyword to ask for nickname

 		client.send('NICK'.encode('ascii')) 
 		nickname = client.recv(1024).decode('ascii')

 		with open('bans.txt', 'r') as b:
 			bans = b.readlines()

 		#Check if nickname is in the bans file
 		if nickname+'\n' in bans:
 			client.send('BAN'.encode('ascii'))
 			client.close()
 			continue

#If someone connects with nickname as admin, prompt for password

 		if nickname == 'admin':
 			client.send('PASS'.encode('ascii'))
 			password = client.recv(1024).decode('ascii')

#Check if password is correct and disconnect client if incorrect
#For more secure pass, open file that contains passwd instead of string

 			if password != 'adminpass':
 				client.send('REFUSE'.encode('ascii'))
 				client.close()
 				continue
#Continue to not perform code below and wait for another client

 		nicknames.append(nickname)
 		clients.append(client)

#Notify if someone joined the chat
 		print(f'Nickname of the client is {nickname}.')
 		send_all(f'{nickname} joined the chat.'.encode('ascii'))

#Notify client that connection has been established
 		client.send('You are now connected to the server.'.encode('ascii'))

#Thread each_client function so that everyone can receive chats simultaneously
 		thread = threading.Thread(target=each_client, args=(client,)) 
 		thread.start()

#Function for kicking/banning and removing name from lists
def kick_user(name):
	if name in nicknames:
		name_index = nicknames.index(name)
		client_to_kick = clients[name_index]
		clients.remove(client_to_kick)
		client_to_kick.send('You were kicked by an admin.'.encode('ascii'))
		client_to_kick.close()
		nicknames.remove(name)
		send_all(f'{name} was kicked by an admin.'.encode('ascii'))



print('Server is listening...')
receive()