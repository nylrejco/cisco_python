
This is a simple DDOS script which will 
slow down or stop a specific port from 
functioning. Do not use this on a device 
or network without permission to do so.