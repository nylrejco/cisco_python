#!/usr/bin/env python

#This script is used to automate sending emails

import smtplib
from email import encoders 
from email.mime.text import MIMEText 
from email.mime.base import MIMEBase 
from email.mime.multipart import MIMEMultipart
from socket import gaierror

#Establish smtp connection

server = smtplib.SMTP('smtp.mailtrap.io', 2525)

server.ehlo()

#Open the file containing the password

with open('password.txt', 'r') as pw:
	password = pw.read()

#Log in to the server using set credentials

server.login('de21fa480ff34d', password)

#Set the header of the email message

msg = MIMEMultipart()
msg['From'] = 'networkprogramming21@gmail.com'
msg['To'] = 'networkprogram21@gmail.com'
msg['Subject'] = 'Test Email'


with open('message.txt', 'r') as m:
	message = m.read()

#Adding message as a text
msg.attach(MIMEText(message, 'plain')) 

#Open in reading byte mode since this is an image data
imgname = 'netprog.png'
attachment = open(imgname, 'rb') 

#Encode image data read and set as payload

po = MIMEBase('application', 'octet-stream')
po.set_payload(attachment.read())
encoders.encode_base64(po) 

#Adding the attachment, adding payload to message

po.add_header('Content-Disposition', f'attachment; filename={imgname}') 
msg.attach(po) 
text = msg.as_string()

#Send email and display any arrors

try:
	server.sendmail('networkprogramming21@gmail.com', 'networkprogram21@gmail.com', text)
	print('Sent')
	
except (gaierror, ConnectionRefusedError):
    print('Failed to connect to the server. Bad connection settings?')
except smtplib.SMTPServerDisconnected:
    print('Failed to connect to the server. Wrong user/password?')
except smtplib.SMTPException as e:
    print('SMTP error occurred: ' + str(e))
