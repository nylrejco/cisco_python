#!/usr/bin/env python

'''Do not scan networks without consent. It is illegal'''

#This script is a simple port scanner

import socket
import threading
from queue import Queue

#Change this to target ip 

target_ip = '172.541.72.145' 
queue = Queue()
open_ports = []

#Establish connection

def port_scanning(port):
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((target_ip, port))
		return True
	except:
		return False

#Go through port numbers
"""for port in range(1, 1024):
	output = port_scanning(port)
	if output is True:
		print("Port number {} is open".format(port))
	else:
		print("Port number {} is closed".format(port)) """

#Put the ports in the list

def fill_queue(port_list):
	for port in port_list:
		queue.put(port)

#Queue the elements and removes element already selected in the list

def port_getter():
	while not queue.empty():
		port = queue.get()
		if port_scanning(port) is True:
			print("Port number {} is open".format(port))
			open_ports.append(port)
		"""else:
			print("Port number {} is closed".format(port))"""

#Assign desired port range

port_list = range(1, 1024)
fill_queue(port_list)

thread_list = [] 

#Only thread the specific target which is port getter

for t in range(10):
	thread = threading.Thread(target=port_getter)
	thread_list.append(thread)

for thread in thread_list:
	thread.start()

#Main thread wait for the queque to finish processing
for thread in thread_list: 
	thread.join() 

print("Open ports are: ", open_ports)